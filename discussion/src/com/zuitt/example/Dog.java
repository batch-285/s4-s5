package com.zuitt.example;

public class Dog extends Animal {
    private String breed;
    public Dog(){
        super();
        this.breed = "Shit-zu";
    };

    public Dog(String name, String color, String breed) {
        super(name, color);
        this.breed = breed;
    }

    public String getBreed() {
        return this.breed = breed;
    }
    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void speak() {
        System.out.println("Aww aww!");
    }
    public void call() {
        super.call();
        System.out.println("Hi, I'm " + this.getName() + ". I am a dog! (from Dog.java)");
    }
}
