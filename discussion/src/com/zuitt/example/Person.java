package com.zuitt.example;

public class Person implements Actions, Greetings {
    public void sleep(){
        System.out.println("Zzzzz");
    };
    public void run(){
        System.out.println("Ruuunnnniiinngg!");
    };

    public void greetings(){
        System.out.println("Good morning!");
    };
    public void holiday(){
        System.out.println("Happy holidays!");
    };
}
