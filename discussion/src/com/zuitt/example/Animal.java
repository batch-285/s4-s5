package com.zuitt.example;

public class Animal {
    private String name;
    private String color;
    public Animal(){};

    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return this.name = name;
    }
    public String getColor() {
        return this.color = color;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setColor(String color) {
       this.color = color;
    }

    public void call() {
        System.out.println("Hi my name is " + this.name + "! (from Animal.java)");
    }

}
