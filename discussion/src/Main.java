import com.zuitt.example.*;

public class Main {
    public static void main(String[] args) {
        Car myCar = new Car();
        myCar.drive();
        myCar.setName("HiAce");
        myCar.setBrand("Toyota");
        myCar.setYearOfMake(2023);
        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ".");

//        Car miniActivity = new Car("Revuelto", "Lamborghini", 2023, "Tinnie");
//        System.out.println("My " + miniActivity.getBrand() + " " + miniActivity.getName() + " was made in " + miniActivity.getYearOfMake() + ". It is driven by " + miniActivity.getDriverName() + ".");

        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ". It is driven by " + myCar.getDriverName() + ".");

        myCar.setDriver("Bitsy");
        System.out.println("My " + myCar.getBrand() + " " + myCar.getName() + " was made in " + myCar.getYearOfMake() + ". It is driven by " + myCar.getDriverName() + ".");

        Dog mypet = new Dog();
        mypet.setName("Mitsui");
        mypet.setColor("White");
        mypet.speak();
        System.out.println("My dog is " + mypet.getName() + " and it is a " + mypet.getColor() + " " + mypet.getBreed() + "!");

        mypet.call();

        Person child = new Person();
        child.sleep();
        child.run();
        child.greetings();
        child.holiday();

        StaticPoly myAdd = new StaticPoly();
        System.out.println(myAdd.addition(5, 6));
        System.out.println(myAdd.addition(5, 6, 7));
        System.out.println(myAdd.addition(5.7, 6.6));

        Child myChild  =  new Child();
        myChild.speak();
    }
}