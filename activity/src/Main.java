import com.zuitt.example.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner contactDetails = new Scanner(System.in);
        String phoneBook;
        List<Person> persons = new ArrayList<>();

        do {
            String fullName = "";
            while (fullName.isEmpty()) {
                System.out.println("Enter your full name:");
                fullName = contactDetails.nextLine();
                if (fullName.isEmpty()) {
                    System.out.println("Invalid input: Full name cannot be empty.");
                }
            }

            Person person = new Person(fullName);

            String homeNumber = "";
            boolean validInput = false;
            while (!validInput) {
                System.out.println("Enter your home number:");
                System.out.print("+63 ");
                homeNumber = contactDetails.nextLine();
                if (homeNumber.isEmpty()) {
                    System.out.println("Input can't be empty, please enter again:");
                } else if (homeNumber.length() == 10 && homeNumber.charAt(0) != '0') {
                    validInput = true;
                } else {
                    System.out.println("Invalid input! (Valid: 10-digit i.e: +63 9123456987):");
                }
            }
            person.setHomeNumber(homeNumber);

            String officeNumber = "";
            validInput = false;
            while (!validInput) {
                System.out.println("Enter your office number:");
                System.out.print("+63 ");
                officeNumber = contactDetails.nextLine();
                if (officeNumber.isEmpty()) {
                    System.out.println("Input can't be empty, please enter again:");
                } else if (officeNumber.length() == 10 && officeNumber.charAt(0) != '0') {
                    validInput = true;
                } else {
                    System.out.println("Invalid input! (Valid: 10-digit i.e: +63 9123456987):");
                }
            }
            person.setOfficeNumber(officeNumber);

            String homeAddress = "";
            while (homeAddress.isEmpty()) {
                System.out.println("Enter your home address:");
                homeAddress = contactDetails.nextLine();
                if (homeAddress.isEmpty()) {
                    System.out.println("Invalid input: Home Address cannot be empty.");
                }
            }
            person.setHomeAddress(homeAddress);

            String officeAddress = "";
            while (officeAddress.isEmpty()) {
                System.out.println("Enter your office address:");
                officeAddress = contactDetails.nextLine();
                if (officeAddress.isEmpty()) {
                    System.out.println("Invalid input: Office Address cannot be empty.");
                }
            }
            person.setOfficeAddress(officeAddress);

            persons.add(person);

            System.out.println("Enter 'q' to quit, or any other key to continue:");
            phoneBook = contactDetails.nextLine();
        } while (!phoneBook.equalsIgnoreCase("q"));

        for (Person person : persons) {
            System.out.println();
            person.displayDetails();
        }
    }
}