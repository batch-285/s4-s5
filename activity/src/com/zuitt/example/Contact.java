package com.zuitt.example;

public interface Contact {
    void setHomeNumber(String homeNumber);
    void setOfficeNumber(String officeNumber);
    void displayPhoneNumbers();
}
