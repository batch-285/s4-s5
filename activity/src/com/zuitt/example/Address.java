package com.zuitt.example;

public interface Address {
    void setHomeAddress(String homeAddress);
    void setOfficeAddress(String officeAddress);
    void displayAddresses();
}
