package com.zuitt.example;

public class Person implements Contact, Address {
    private String fullName;
    private String homePhoneNumber;
    private String officePhoneNumber;
    private String homeAddress;
    private String officeAddress;

    public Person(String fullName) {
        this.fullName = fullName;
    }

    public void setHomeNumber(String homeNumber) {
        this.homePhoneNumber = homeNumber;
    }
    public void setOfficeNumber(String officeNumber) {
        this.officePhoneNumber = officeNumber;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }
    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public void displayPhoneNumbers() {
        System.out.println(fullName + " has the following registered numbers:");
        System.out.println("Home: +63" + homePhoneNumber);
        System.out.println("Office: +63" + officePhoneNumber);
    }

    public void displayAddresses() {
        System.out.println(fullName + " has the following registered addresses:");
        System.out.println("Home: " + homeAddress);
        System.out.println("Office: " + officeAddress);
    }

    public void displayDetails() {
        System.out.println("====================================================");
        System.out.println(fullName);
        System.out.println("----------------------------------------------------");
        displayPhoneNumbers();
        System.out.println("----------------------------------------------------");
        displayAddresses();
        System.out.println("====================================================");
    }

}
